package app

import (
	"net/http"
	"strconv"
)

type App struct {
}

func (a *App) Run() {
	http.HandleFunc("/", handleIndex)
	http.ListenAndServe("localhost:8080", nil)
}

func handleIndex(w http.ResponseWriter, r *http.Request) {
	num := r.FormValue("num")
	n, err := strconv.Atoi(num)
	if err != nil {
		http.Error(w, "input num is not int", http.StatusBadRequest)
		return
	}
	w.Write([]byte(strconv.Itoa(n)))
}

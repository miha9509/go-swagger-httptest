package app

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestHandleHello(t *testing.T) {
	testClasses := []struct {
		name string
		num  interface{}
		want []byte
	}{
		{
			name: "zero",
			num:  0,
			want: []byte("0"),
		},
		{
			name: "one",
			num:  -1,
			want: []byte("-1"),
		},
		{
			name: "string",
			num:  "3",
			want: []byte("input num is not int\n"),
		},
	}

	handler := http.HandlerFunc(handleIndex)

	for _, tc := range testClasses {
		t.Run(tc.name, func(t *testing.T) {
			rec := httptest.NewRecorder()
			req, _ := http.NewRequest("GET", fmt.Sprintf("/?num=%d", tc.num), nil)
			handler.ServeHTTP(rec, req)
			assert.Equal(t, tc.want, rec.Body.Bytes())
		})
	}
}

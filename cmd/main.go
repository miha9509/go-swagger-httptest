package main

import "testapp/pkg/app"

func main() {
	app := app.App{}
	app.Run()
}
